from app import db


class DataTable(db.Model):
    __tablename__ = "datatable"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    distance = db.Column(db.Integer)
    litres = db.Column(db.Float)
    date = db.Column(db.DateTime)
