from app import db
from app.models import DataTable


def add_entry(kms, user_id):
    query = DataTable.query.filter(DataTable.user_id == user_id and DataTable.litres is not None)
    if query.count() == 0:
        data_table = DataTable()
        data_table.user_id = user_id
        data_table.distance = kms
        db.session.add(data_table)
        db.session.commit()
        return "Added %s kms successfully" % kms
    else:
        return False


def update_litres(litres, user_id):
    query = DataTable.query.filter(DataTable.user_id == user_id and DataTable.litres is None)
    if query.count() == 1:
        query.update({
            DataTable.litres: litres
        })
        kms = query.all()[0].kms
        db.session.commit()
        return "Added %s litres for %s kms" % (litres, kms)
    else:
        return False


def get_average(user_id):
    query = DataTable.query.filter(DataTable.user_id == user_id)
    if query.count() > 1:
        data_1 = query.order_by(DataTable.id.desc()).all()[0]
        data_2 = query.order_by(DataTable.id.desc()).all()[1]
        distance_delta = data_1.distance - data_2.distance
        litres_delta = data_2.litres
        return "Average %s " % (distance_delta / litres_delta)
    else:
        return "You need some more data to get averaged"


def clear_for_user_id(user_id):
    query = DataTable.query.filter(DataTable.user_id == user_id)
    if query.count() > 0:
        for data_ in query.all():
            db.session.delete(data_)
        db.session.commit()
        return "Data cleared for your user id"
    else:
        return "No data found"


def create_return_payload(text_return, chat):
    return_json = {'method': 'sendMessage', 'chat_id': chat.get('id'), 'parse_mode': "markdown"}
    if text_return:
        return_json['text'] = text_return
    else:
        return_json['text'] = "Something is *wrong* with message"

    return return_json


def process_message(message):
    text = message.get('text', None)
    user = message.get('from', None)
    chat = message.get('chat', None)
    if user is not None:
        user_id = user.get('id', 0)
        if '/ltrs' in text:
            litres = text.split('ltrs')[1]
            text_return = update_litres(int(litres), int(user_id))
        elif '/kms' in text:
            kms = text.split('kms')[1]
            text_return = add_entry(int(kms), int(user_id))
        elif '/avg' in text:
            text_return = get_average(user_id)
        elif '/clear' in text:
            text_return = clear_for_user_id(user_id)
        else:
            text_return = "You seem to have missed something /help for more details"
        if text_return is not None:
            return create_return_payload(text_return, chat)
