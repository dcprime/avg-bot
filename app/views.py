from json import dumps

from flask import request, make_response

from app import app
from functions import process_message


@app.route('/bot/<token>', methods=['POST'])
def bot(token):
    if token not in app.config.get('BOT_TOKENS'):
        return "{}", 200
    try:
        if 'message' in request.json:
            send_response = process_message(request.json.get('message'))
            if send_response is not None:
                response = make_response(dumps(send_response))
                response.headers['Content-Type'] = "application/json"
                return response, 200
    except Exception as e:
        app.logger.error(e)
        return "{}", 200
